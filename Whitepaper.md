**OnePass WhitePaper**

OnePass is a cloud based password manager for enterprises. The service is hosted on secure servers and distributed across
locations. This allows for easy access from any device via internet browser or specific apps (later release). 
We do not store any raw passwords, but store hashed and salted versions associated with your accounts. This
means that even if we were to be breached, our data would not be readable to humans. Passwords could in theory be cracked,
however we make use of SHA256 hashing as well as salting to prevent against rainbow table attacks.