sad-project
-----------------------------------
Our group task, due in week 6 for System and Software Analysis and Design.


**OnePass**
===================================



Our project is called OnePass.

It is a system which is based on an application which allows them to remotely access all their passwords, from any type of device.
The application will run in the background of the OS it is running on (Windows, Android, iOS, etc),
and automatically fill in the password fields when it identifies a website that the user has saved a password for.

The passwords will be stored on OnePass owned servers, this allows users to access their passwords from anywhere provided they have internet.
The users access their passwords whilst logging into the application using their OnePass email and password. 
Users can then access their passwords in the application or the app will automatically fill in the passwords for websites that passwords have been stored for.

The application will also be able to generate complex passwords for users automatically, as the user will not need to remember them.
>>>>>>> master
